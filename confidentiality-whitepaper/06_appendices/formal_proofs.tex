\section{Formal Proofs}
\label{Appendix B}
\subsection{Literature Review}

The Xand Network provides a business-to-business on-demand settlement solution that maintains a digital ledger using a blockchain. This distributed ledger tracks Xand Claims on funds held in trust. The Xand Network is permissioned and has important governance requirements about what entities can act as Members. Additionally, Members require some level of privacy on their financial transactions. In this section we give a literature review for the techniques used in the confidentiality protocol for the Xand Network.

Our Xand confidentiality protocol is designed based on techniques pioneered by Monero, which is a standard one-dimensional distributed cryptocurrency blockchain \cite{nakamoto2019bitcoin}. Monero was initially known as CryptoNote \cite{van2013cryptonote} published under the pseudonym of Nicolas van Saberhagen. It offered receiver anonymity through the use of one-time addresses, and sender ambiguity by means of ring signatures. Since its appearance, Monero has further strengthened its privacy aspects by implementing amount hiding, as described by Greg Maxwell in \cite{maxwell2015borromean}, integrating ring signatures based on Shen Noether's recommendations in \cite{noether2016ring}, and made more efficient with Bulletproofs \cite{bunz2018bulletproofs}. Afterwards, there are several version updates called \textit{Zero to Monero} that keep improving cryptographic primitives used \cite{alonso2018monero,alonso2020zero}.

The Xand Confidentiality Protocol has leveraged many of the advantages of Monero while adding to the protocol with our governance requirements. Transactions in our protocol are based on elliptic curve cryptography using curve Ed25519\cite{bernstein2008twisted} and transaction inputs are signed with Schnorr-style ring signature\cite{noether2016ring}, and output amounts are communicated to recipients via Elliptic Curve Diffie-Hellman(ECDH)\cite{diffie1976new} and concealed with Pedersen commitments \cite{maxwell2015confidential} and proven in a legitimate range with Bulletproofs \cite{bunz2018bulletproofs}. We use a special curve belonging to the category of so-called \textit{Twisted Edwards} curves \cite{bernstein2008twisted}. In particular, it uses Curve25519 which was released in \cite{bernstein2008twisted}. The advantage this special curve offers is that basic cryptographic primitives require fewer arithmetic operations, resulting in faster cryptographic algorithms. More details are described in Bernstein et al.\cite{bernstein2007faster}.

A zero-knowledge proof system is a key part of our confidentiality protocol. The notion of a zero-knowledge proof system was introduced by Goldwasser, Micali and Rackoff \cite{goldwasser1989knowledge} which is central in cryptography. Since its introduction, the original definition has been then reformulated under several variations, trying to capture additional security and efficiency guarantees. In recent years, the concept of a zero knowledge proof has been extremely influential and useful for many other notions and applications in blockchain-based financial systems. Some notable examples are ZCash \cite{hopwood2016zcash}, Hyperledger Fabric \cite{androulaki2018hyperledger}, Ethereum \cite{wood2014ethereum} and Cardano's Ouroboros \cite{kerber2019ouroboros}.

We have designed our own zero-knowledge proof protocol called \textit{Zero-Knowledge Proof for Linear Member Tuple (ZKPLMT)}, which is a more generalized version to support proving a member's identity in a set of members. The protocol is based on the Schnorr Identification Protocol \cite{schnorr1989efficient, maurer2009unifying}. Identification, also known as entity authentication, is a process by which a verifier gains assurance that the identity of a prover is as claimed, i.e., there is no impersonation. An identification scheme enables a prover holding a secret key to identify itself to a verifier holding the corresponding public key. The origin Schnorr identification protocol is interactive and not publicly verifiable, we use Fiat-Shamir transform \cite{fiat1986prove}. That is, we assume the hash function as a random oracle to generate challenges in the interactive proof protocol. Our ZKPLMT protocol follows the paradigm of Schnorr Identification Protocol to construct non-interactive zero-knowledge proofs of knowledge. These are protocols which allow a prover to demonstrate knowledge of a secret while revealing no information whatsoever other than the one bit of information regarding the possession of the secret \cite{feige1988zero, goldwasser1989knowledge}. In our use-cases, we particularly focus on its application that it allows someone to prove they know the private key \(k\) of a given public key \(K\) without revealing any information about it \cite{maxwell2015borromean}.

\subsection{Prelimaries}
\subsubsection{Notation}
\begin{itemize}
    \item The left arrow ($\leftarrow$) signifies either assignment or computation or random selection. The notation $X \leftarrow Y$ means assign the value of $Y$ to the variable $X$ when $Y$ is an expression. If $\mathcal{S}$ is a set, $X \leftarrow \mathcal{S}$ represents choosing $X$ uniformly over $\mathcal{S}$. If $\mathcal{A}$ is an algorithm, $X \leftarrow \mathcal{A}$ represents running $\mathcal{A}$ once and assigning the result to $X$.
    \item We use $i$ as the index of a component of a tuple, $j$ to index a tuple in a set.
    \item We will use the operator $\forall$ to signify a sequence indexed by a variable. For example, $\forall_j (X_j, Y_j)$ represents $(X_1,Y_1), (X_2,Y_2), ...$ for all values of $j$. $\forall_{j=1}^m (c_j,d_j)$ represents $(c_1,d_1),(c_2,d_2),...,(c_m,d_m)$. Similarly, for a set $\mathcal{S}$, $\forall_{j \in \mathcal{S}} X_j$ represents a sequence of $X_j$ for all values of $j \in \mathcal{S}$.
          Notice that we use subscript and superscript with $\forall$ for this custom notation. When used with regular text position, $\forall$ has the usual meaning.
    \item $\mathbb{G}$ is an elliptic curve group of prime order $q$. We assume that $q$ is close to $2^{\lambda}$ where $\lambda$ is the security parameter.
    \item $G$ is a system-wide fixed generator of $\mathbb{G}$.
    \item $L$ is a system-wide fixed generator of $\mathbb{G}$ such that the discrete logarithm of $G$ with respect to $L$ is not known.
    \item $\mathbb{F}_q$ is the field integer modulo $q$.
    \item $\mathbb{F}^*_q$ is $\mathbb{F}_q \setminus \{0\}$.
    \item $H_g$ is a hash function with output in the group.
    \item $H_q$ is a hash function with output in $\mathbb{F}_q^*$.
    \item $H_b$ is a hash function with output in $\{0,1\}^b$ for a fixed $b$.
    \item $\phi$ is an empty string of bits or an empty message.
    \item Bold $\mathbf{P}$ is the prover algorithm for ZkPLMT.
    \item Bold $\mathbf{V}$ is the verification algorithm for ZkPLMT.
\end{itemize}

\subsubsection{Definitions}

\begin{definition}{Negligible function}
    A function $f:\mathbb{N} \rightarrow \mathbb{R}^+$ is negligible in $\lambda$ if for every polynomial $\mathbf{p}$, there exists $\mathbf{n}$ such that $f(\lambda) < \frac{1}{\mathbf{p}}$ whenever $\lambda \ge \mathbf{n}$.
\end{definition}

\begin{definition}{DDH Assumption} We assume the DDH problem is hard to solve in the group $\mathbb{G}$. The advantage of a DDH adversary $\mathcal{C}$ is given as -

    $Adv^{DDH}(\mathcal{C}) =$

    \[\Biggl\lvert
        Pr\left[
            \begin{array}{ll}
                \rho=1
            \end{array} \left|
            \begin{array}{ll}
                G \leftarrow \mathbb{G}  ;       \\
                (x,y) \leftarrow \mathbb{F}_q^2; \\
                \rho =                           \\
                \spc \mathcal{C}(G,xG,yG,xyG);
            \end{array}
            \right.
            \right]\\
    \]
    \[
        -
        Pr\left[
            \begin{array}{ll}
                \rho=1
            \end{array} \left|
            \begin{array}{ll}
                G \leftarrow \mathbb{G}  ;       \\
                (x,y) \leftarrow \mathbb{F}_q^2; \\
                R \leftarrow \mathbb{G};         \\
                \rho =                           \\
                \spc \mathcal{C}(G,xG,yG,R);
            \end{array}
            \right.
            \right]\Biggl\rvert\\
    \]
\end{definition}
We assume that the advantage of any PPTA $\mathcal{C}$ is negligible in the security parameter $\lambda$.

\input{confidentiality-whitepaper/06_appendices/formal_requirements.tex}