The papers are deployed to the following location - 
https://transparentincdevelopment.gitlab.io/xand-yellow-paper-latex/confidentiality-whitepaper.pdf
https://transparentincdevelopment.gitlab.io/xand-yellow-paper-latex/technical-whitepaper.pdf

To build using native latex installation
latexmk -pdf

It is setup to handle any other latex tool

licensed under MIT OR Apache-2.0
